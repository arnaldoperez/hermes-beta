module.exports = {
  transpileDependencies: [
    'vuetify'
  ],

  pwa: {
    name: 'TiendaFlex'
  }
}
