import firebase from 'firebase/app'
import store from '../store'
import firebaseConfig from './firebaseConfig'

export default function firebaseInit () {
  firebase.initializeApp(firebaseConfig)
  firebase.auth().onAuthStateChanged(user => {
    store.commit('changeUser', user)
  })
  firebase.auth().useDeviceLanguage()
}
