import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import filters from './filters'
import mixins from './mixins'
import firebaseInit from './firebaseApp/init'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import './registerServiceWorker'

Vue.config.productionTip = false
filters()
new Vue({
  router,
  store,
  vuetify,
  mixins,
  created () {
    firebaseInit()
  },
  render: h => h(App)
}).$mount('#app')
