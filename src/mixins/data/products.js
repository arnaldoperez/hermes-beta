import { db } from '../../firebaseApp/firestore'

export default {
  created () {
    db.collection('products').where('category', '==', this.category).onSnapshot(snapshot => {
      this.products = snapshot.docs.map(data => {
        var doc = data.data()
        if (!doc.img || doc.img === 'default') doc.img = require('../../assets/categoria.png')
        doc.id = data.id
        return doc
      })
    })
  },
  data () {
    return {
      products: []
    }
  },
  props: {
    category: {
      defualt: ''
    }
  }
}
