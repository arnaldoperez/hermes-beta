export default {
  state: {
    userInfo: {},
    cartItems: []
  },
  mutations: {
    changeUser (state, user) {
      if (!user) {
        state.userInfo = {}
        return
      }
      state.userInfo.displayName = user.displayName
      state.userInfo.email = user.email
      state.userInfo.photoURL = user.photoURL
      state.userInfo.uid = user.uid
    },
    addCartItem (state, item) {
      // Busca el index del item a insertar
      const itemIndex = state.cartItems.findIndex(val => val.id === item.id)
      // Si no lo encuentra, lo agrega
      if (itemIndex === -1) {
        state.cartItems.push(item)
        return
      }
      // Si trae una cantidad especifica, sobreescribe la cantidad
      if (item.quantity) {
        state.cartItems.cartItems[itemIndex].quantity = item.quantity
      } else { // Si no trae una cantidad especifica, se agrega uno solo
        state.cartItems.cartItems[itemIndex].quantity++
      }
    }
  },
  actions: {}
}
